# Expensify

This is my version of the Expensify app from Andrew Mead's React course on Udemy.

Occasionally, my version will feature ECMA script and SASS features Mead did not use.

## Babel

To use (without Webpack) use `yarn build`

## Notes

Yarn registry is https://registry.yarnpkg.com