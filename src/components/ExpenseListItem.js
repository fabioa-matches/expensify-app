import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import numeral from 'numeral';

export const ExpenseListItem = ({ description, amount, createdAt, id }) => (
  <li>
    {description}: {numeral(amount / 100).format('$0,0.00')}
    , created on {moment(createdAt).format('MMMM Do, YYYY @ HH:mm')}
    <span>
      <Link to={`/edit/${id}`}>Edit</Link>
    </span>
  </li>
);

export default ExpenseListItem;
