import React from 'react';
import { connect } from 'react-redux';

import ExpenseListItem from './ExpenseListItem';
import selectExpenses from '../selectors/expenses';

export const ExpenseList = props => (
  <div>
    <h3>Expense List</h3>
    <ul>
      {
        props.expenses.length === 0 ? (
          <li>No expenses found</li>
        ) : (
          props.expenses.map(expense => (
            <ExpenseListItem key={expense.id} {...expense} />
          ))
        )
      }
    </ul>
  </div>
);

const mapStateToProps = ({ expenses, filters }) => ({
  expenses: selectExpenses(expenses, filters),
});

export default connect(mapStateToProps)(ExpenseList);
