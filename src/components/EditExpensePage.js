import React, { Component } from 'react';
import { connect } from 'react-redux';

import ExpenseForm from './ExpenseForm';
import { editExpense, removeExpense } from '../actions/expenses';

export class EditExpensePage extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onRemove = this.onRemove.bind(this);
  }
  onSubmit(expenseA) {
    this.props.editExpense(this.props.expense.id, expenseA);
    this.props.history.push('/');
  }
  onRemove() {
    this.props.removeExpense({ id: this.props.expense.id });
    this.props.history.push('/');
  }
  render() {
    return (
      <div>
        <h3>Edit Expense</h3>
        <ExpenseForm
          expense={this.props.expense}
          onSubmit={this.onSubmit}
        />
        <button
          onClick={this.onRemove}
        >X</button>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch, props) => ({
  editExpense: (id, expense) => dispatch(editExpense(id, expense)),
  removeExpense: expenseData => dispatch(removeExpense(expenseData)),
});

const mapStateToProps =
  ({ expenses }, props) => (
    { expense: expenses.find(expense => expense.id === props.match.params.id) }
  );


export default connect(mapStateToProps, mapDispatchToProps)(EditExpensePage);
