import React from 'react';
import { connect } from 'react-redux';
import numeral from 'numeral';

import getExpensesTotal from '../selectors/expensesTotal';
import selectExpenses from '../selectors/expenses';

export const ExpensesSummary = ({ expensesCount, expensesTotal }) => (
  <div>
    <p>Viewing {expensesCount === 1 ? '1 expense' : `${expensesCount} expenses`} totaling {numeral(expensesTotal / 100).format('$0,0.00')}</p>
  </div>
);

const mapStateToProps = ({ expenses, filters }) => {
  const visibleExpenses = selectExpenses(expenses, filters);
  return {
    expensesTotal: getExpensesTotal(visibleExpenses),
    expensesCount: visibleExpenses.length,
  };
};

export default connect(mapStateToProps)(ExpensesSummary);
