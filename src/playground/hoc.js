import React from 'react';
import ReactDOM from 'react-dom';

// generic component
const Info = props => (
  <div>
    <h1>Info</h1>
    <p>Lorem ipsum dolor sit amet: {props.info}</p>
  </div>
);

// HOC returned
const withAdmin = (WrappedComponent) => {
  return props => (
    <div>
      {props.isAdmin && <p>Priviledged information, do not share.</p>}
      <WrappedComponent {...props} />
    </div>
  );
};

const requireAuth = (WrappedComponent) => {
  return props => (
    <div>
      {props.isAuthenticated ? <WrappedComponent {...props} /> : <p>Please log in.</p>}
    </div>
  );
};

const AdminInfo = withAdmin(Info);
const AuthInfo = requireAuth(Info);

ReactDOM.render(<AuthInfo isAuthenticated info="What is this" />, document.getElementById('app'));
