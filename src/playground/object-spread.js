const usr = {
  name: 'Phnuphna',
  age: 56,
};

// age is an override
console.log({ ...usr, location: 'Penn', age: 44 });
