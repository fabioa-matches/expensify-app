import * as firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyBBOR3EXwsOL47cJRQotoEFnAGRCIYR9Os',
  authDomain: 'expensify-3f402.firebaseapp.com',
  databaseURL: 'https://expensify-3f402.firebaseio.com',
  projectId: 'expensify-3f402',
  storageBucket: 'expensify-3f402.appspot.com',
  messagingSenderId: '588437110555',
};

firebase.initializeApp(config);

const db = firebase.database();

db.ref('expenses').on('child_removed', (snapshot) => {
  console.log(snapshot.key, snapshot.val());
});

db.ref('expenses').on('child_changed', (snapshot) => {
  console.log(snapshot.key, snapshot.val());
});

// db.ref('expenses').on('value', (snapshot) => {
//   const expenses = [];

//   snapshot.forEach((childSnapshot) => {
//     expenses.push({
//       id: childSnapshot.key,
//       ...childSnapshot.val(),
//     });
//   });

//   console.log(expenses);
// });

// db.ref('expenses')
//   .once('value')
//   .then((snapshot) => {
//     const expenses = [];

//     snapshot.forEach((childSnapshot) => {
//       expenses.push({
//         id: childSnapshot.key,
//         ...childSnapshot.val(),
//       });
//     });

//     console.log(expenses);
//   })
//   .catch((e) => {
//     console.log('Error fetching data:', e);
//   });

// db.ref('expenses').push({
//   description: 'Test expense',
//   note: 'Just testing',
//   amount: 3345,
//   createdAt: 1150,
// });

// db.ref('expenses').push({
//   description: 'Another expense',
//   note: 'Just testing again',
//   amount: 3455,
//   createdAt: 11250,
// });

// db.ref('expenses').push({
//   description: 'Test expense 3',
//   note: 'Just testing two',
//   amount: 4355,
//   createdAt: 22150,
// });

// db.ref('notes').push({
//   title: 'Some topics',
//   body: 'Wallies, wellies, boots, chemistry',
// });

// db.ref('notes').push({
//   title: 'Nonsense makes',
//   body: 'Willy wonka is one',
// });

// Fetching data

// Retrieve
// db.ref()
//   .once('value')
//   .then((snapshot) => {
//     console.log(snapshot.val());
//   })
//   .catch((e) => {
//     console.log('Error fetching data:', e);
//   });

// Subscribe

// db.ref().on('value', (snapshot) => {
//   const data = snapshot.val();

//   console.log(`${data.name} is a ${data.job.title} at ${data.job.company}.`);
// }, (e) => {
//   console.log('Error with fetching:', e);
// });

// db.ref().on('value', (snapshot) => {
//   console.log(snapshot.val());
// }, (e) => {
//   console.log('Error with fetching:', e);
// });

// setTimeout(() => {
//   db.ref('name').set('Dave');
// }, 3500);

// setTimeout(() => {
//   db.ref('age').set(45);
// }, 3500);

// end all subs

// setTimeout(() => {
//   db.ref().off();
// }, 7000);

// setTimeout(() => {
//   db.ref('age').set(47);
// }, 10500);

// db.ref().set({
//   name: 'Test user',
//   age: 44,
//   stressLevel: 5,
//   isAwesome: true,
//   job: {
//     title: 'Head of Product',
//     company: 'Google',
//   },
//   location: {
//     city: 'Buenos Aires',
//     country: 'Argentina',
//   },
// }).then(() => {
//   console.log('Data has been set');
// }).catch((err) => {
//   console.log('Data set failed: ', err);
// });

// db.ref().update({
//   stressLevel: 9,
//   nick: 'juerb',
//   isAwesome: null,
//   'job/company': 'Amazon',
//   'location/city': 'Porto Allegre',
// });

// db.ref('isAwesome').remove()
//   .then(() => {
//     console.log('Removal successful');
//   }).catch((err) => {
//     console.log('Removal failed: ', err);
//   });
