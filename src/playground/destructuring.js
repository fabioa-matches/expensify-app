// Objects

console.log('destruct Turing');

const person = {
  // name: 'Phil',
  employer: 'Adelphia',
  location: {
    city: 'Baltimore',
    temperature: 55,
  },
};

const { name = 'A Nom Y\'Mouse', employer } = person;

console.log(`${name} from ${employer}`);

const { city, temperature: temp } = person.location;

console.log(`It is ${temp}F in ${city}`);

const book = {
  title: 'Odissey',
  author: 'Homer',
  publisher: {
    name: 'Random House',
  },
};

const { name: publisherName = 'Self-Published' } = book.publisher;

console.log(publisherName);

// Arrays

const address = ['345 Strings Blvd.', 'Springfield', 'IA', '24336'];

const [, city1, state, , country = 'USA'] = address;

console.log(`You are in ${city1}, ${state}, ${country}.`);

const item = ['Coffee (hot)', '$2.00', '$3.00', '$4.50'];

const [theItem, , price] = item;

console.log(`A medium ${theItem} costs ${price}`);
