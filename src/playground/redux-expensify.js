import { createStore, combineReducers } from 'redux';
import uuid from 'uuid';

// action creators
const addExpense = ({
  description = '',
  note = '',
  amount = 0,
  createdAt = 0,
} = {}) => ({
  type: 'ADD_EXPENSE',
  expense: {
    id: uuid(),
    description,
    note,
    amount,
    createdAt,
  },
});

const removeExpense = ({ id } = {}) => ({
  type: 'REMOVE_EXPENSE',
  id,
});

const editExpense = (id, updates) => ({
  type: 'EDIT_EXPENSE',
  id,
  updates,
});

const setTextFilter = (text = '') => ({
  type: 'SET_TEXT_FILTER',
  text,
});

const sortByAmount = () => ({
  type: 'SORT_BY_AMOUNT',
});

const sortByDate = () => ({
  type: 'SORT_BY_DATE',
});

const setStartDate = date => ({
  type: 'SET_START_DATE',
  date,
});

const setEndDate = date => ({
  type: 'SET_END_DATE',
  date,
});

const reducerDefaults = {
  expenses: [],
  filters: {
    text: '',
    sortBy: 'date',
    startDate: undefined,
    endDate: undefined,
  },
};

const expensesReducer = (state = reducerDefaults.expenses, action) => {
  switch (action.type) {
    case 'ADD_EXPENSE':
      return [...state, action.expense];
    case 'REMOVE_EXPENSE':
      return state.filter(({ id }) => id !== action.id);
    case 'EDIT_EXPENSE':
      return state.map((expense) => {
        if (expense.id === action.id) {
          return { ...expense, ...action.updates };
        }

        return expense;
      });
    default:
      return state;
  }
};

const filtersReducer = (state = reducerDefaults.filters, action) => {
  switch (action.type) {
    case 'SET_TEXT_FILTER':
      return { ...state, text: action.text };
    case 'SORT_BY_AMOUNT':
      return { ...state, sortBy: 'amount' };
    case 'SORT_BY_DATE':
      return { ...state, sortBy: 'date' };
    case 'SET_START_DATE':
      return { ...state, startDate: action.date };
    case 'SET_END_DATE':
      return { ...state, endDate: action.date };
    default:
      return state;
  }
};

const getVisibleExpenses = (expenses, { text, sortBy, startDate, endDate }) => {
  return expenses.filter((expense) => {
    const startDateMatch = typeof startDate !== 'number' || expense.createdAt >= startDate;
    const endDateMatch = typeof endDate !== 'number' || expense.endDate <= endDate;
    const textMatch = typeof text !== 'string' || expense.description.toLowerCase().includes(text.toLowerCase());

    return startDateMatch && endDateMatch && textMatch;
  }).sort((a, b) => {
    if (sortBy === 'date') {
      return a.createdAt < b.createdAt ? 1 : -1;
    } else if (sortBy === 'amount') {
      return a.amount < b.amount ? 1 : -1;
    }

    return undefined;
  });
};

const store = createStore(
  combineReducers({
    expenses: expensesReducer,
    filters: filtersReducer,
  }),
);

store.subscribe(() => {
  const state = store.getState();
  const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);

  console.log(visibleExpenses);
});

const exp1 = store.dispatch(addExpense({ description: 'A sample expense', amount: 1121, createdAt: 1000 }));
const exp2 = store.dispatch(addExpense({ description: 'Another sample expense', amount: 2121, createdAt: -1000 }));
const exp3 = store.dispatch(addExpense({ description: 'Some sample expense', amount: 21223, createdAt: 1200 }));

// store.dispatch(removeExpense({ id: exp1.expense.id }));
// store.dispatch(editExpense(exp2.expense.id, { amount: 55651 }));
store.dispatch(setTextFilter('sample'));

// --reset
// store.dispatch(setTextFilter());

// --date is default, so amount first
// store.dispatch(sortByAmount());
store.dispatch(sortByDate());

// store.dispatch(setStartDate(125));
// --reset
// store.dispatch(setStartDate());
// store.dispatch(setEndDate(1351));


const stateTemplate = {
  expenses: [{
    id: 'mn3542',
    description: 'Albatross',
    note: 'It\'s not any flavour.',
    amount: 54000,
    createdAt: 0,
  }],
  filters: {
    text: 'animal',
    sortBy: 'amount', // can be date or amount
    startDate: undefined,
    endDate: undefined,
  },
};
