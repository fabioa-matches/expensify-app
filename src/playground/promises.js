const promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('Resolved promise data');
    // reject('Oops');
  }, 2000);
});

// on resolve:
promise.then((data) => {
  console.log(data);

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('Resolved second promise data');
    }, 2000);
  });
}).then((str) => {
  console.log('Does this run?', str);
})
  .catch((error) => {
    console.log('Error: ', error);
  });
