import React from 'react';
import { shallow } from 'enzyme';

import { ExpensesSummary } from '../../components/ExpensesSummary';

test('should correctly render ExpensesSummary with two expenses', () => {
  const wrapper = shallow(<ExpensesSummary expenseCount={5} expensesTotal={1235} />);

  expect(wrapper).toMatchSnapshot();
});

test('should correctly render ExpensesSummary with one expense', () => {
  const wrapper = shallow(<ExpensesSummary expenseCount={1} expensesTotal={235} />);

  expect(wrapper).toMatchSnapshot();
});
