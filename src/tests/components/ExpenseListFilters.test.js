import React from 'react';
import moment from 'moment';
import { shallow } from 'enzyme';

import { ExpenseListFilters } from '../../components/ExpenseListFilters';

import { filters, popFilters } from '../fixtures/filters';

let setTextFilter;
let sortByDate;
let sortByAmount;
let setStartDate;
let setEndDate;
let wrapper;

beforeEach(() => {
  setTextFilter = jest.fn();
  sortByDate = jest.fn();
  sortByAmount = jest.fn();
  setStartDate = jest.fn();
  setEndDate = jest.fn();

  wrapper = shallow(
    <ExpenseListFilters
      filters={filters}
      setTextFilter={setTextFilter}
      sortByDate={sortByDate}
      sortByAmount={sortByAmount}
      setStartDate={setStartDate}
      setEndDate={setEndDate}
    />,
  );
});

test('should render ExpenseListFilters correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

test('should render ExpenseListFilters with populated filters correctly', () => {
  wrapper.setProps({
    filters: popFilters,
  });
  expect(wrapper).toMatchSnapshot();
});

test('should handle text change', () => {
  const value = 'johns';
  // const event = { target: { value: 'johns' } };
  wrapper.find('input').simulate('change', {
    target: { value },
  });

  expect(setTextFilter).toHaveBeenLastCalledWith(value);
});

test('should sort by date', () => {
  // const event = { target: { value: 'date' } };
  const value = 'date';

  wrapper.setProps({
    filters: popFilters,
  });
  wrapper.find('select').simulate('change', {
    target: { value },
  });

  expect(sortByDate).toHaveBeenCalled();
});

test('should sort by amount', () => {
  // const event = { target: { value: 'amount' } };
  const value = 'amount';

  wrapper.find('select').simulate('change', {
    target: { value },
  });

  expect(sortByAmount).toHaveBeenCalled();
});

test('should handle date changes', () => {
  const startDate = moment(0).add(2, 'months');
  const endDate = moment(0).add(6, 'months');

  wrapper.find('withStyles(DateRangePicker)').prop('onDatesChange')({ startDate, endDate });

  expect(setStartDate).toHaveBeenLastCalledWith(startDate);
  expect(setEndDate).toHaveBeenLastCalledWith(endDate);
});

test('should handle date focus changes', () => {
  const calendarFocused = 'endDate';

  wrapper.find('withStyles(DateRangePicker)').prop('onFocusChange')(calendarFocused);

  expect(wrapper.state('calendarFocused')).toBe(calendarFocused);
});
