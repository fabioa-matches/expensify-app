import moment from 'moment';

import expensesReducer from '../../reducers/expenses';
import expenses from '../fixtures/expenses';

test('should set up default expense state', () => {
  const state = expensesReducer(undefined, { type: '@@INIT' });

  expect(state).toEqual([]);
});

test('should remove expense by id', () => {
  const action = {
    type: 'REMOVE_EXPENSE',
    id: expenses[1].id,
  };
  const state = expensesReducer(expenses, action);

  expect(state).toEqual([expenses[0], expenses[2]]);
});

test('should not remove expense with invalid id', () => {
  const action = {
    type: 'REMOVE_EXPENSE',
    id: '4',
  };
  const state = expensesReducer(expenses, action);

  expect(state).toEqual(expenses);
});

test('should add an expense', () => {
  const newExpense = {
    id: '5',
    description: 'Sugar',
    note: '',
    amount: 245,
    createdAt: moment(86400).add(5, 'days').valueOf(),
  };
  const action = {
    type: 'ADD_EXPENSE',
    expense: newExpense,
  };
  const state = expensesReducer(expenses, action);

  expect(state).toEqual([...expenses, newExpense]);
});

test('should edit an expense', () => {
  const action = {
    type: 'EDIT_EXPENSE',
    id: expenses[2].id,
    updates: {
      amount: 2232,
    },
  };
  const state = expensesReducer(expenses, action);

  expect(state[2]).toEqual({ ...expenses[2], ...action.updates });
});

test('should not edit expense with invalid id', () => {
  const action = {
    type: 'EDIT_EXPENSE',
    id: '65',
    updates: {
      amount: 1005,
    },
  };
  const state = expensesReducer(expenses, action);

  expect(state).toEqual(expenses);
});

test('should set expenses', () => {
  const action = {
    type: 'SET_EXPENSES',
    expenses: [expenses[1]],
  };

  const state = expensesReducer(expenses, action);

  expect(state).toEqual([expenses[1]]);
});
