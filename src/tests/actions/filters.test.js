import moment from 'moment';

import { setStartDate, setEndDate, sortByAmount, sortByDate, setTextFilter } from '../../actions/filters';

test('should generate setStartDate action object', () => {
  const action = setStartDate(moment(12345));

  expect(action).toEqual({
    type: 'SET_START_DATE',
    date: moment(12345),
  });
});

test('should generate setEndDate action object', () => {
  const action = setEndDate(moment(678910));

  expect(action).toEqual({
    type: 'SET_END_DATE',
    date: moment(678910),
  });
});

test('should generate sortByAmount action object', () => {
  const action = sortByAmount();

  expect(action).toEqual({
    type: 'SORT_BY_AMOUNT',
  });
});

test('should generate sortByDate action object', () => {
  const action = sortByDate();

  expect(action).toEqual({
    type: 'SORT_BY_DATE',
  });
});

test('should generate setTextFilter action object with provided values', () => {
  const textFilterValue = 'Swan Lake';
  const action = setTextFilter(textFilterValue);

  expect(action).toEqual({
    type: 'SET_TEXT_FILTER',
    text: textFilterValue,
  });
});

test('should generate setTextFilter action object with default values', () => {
  const action = setTextFilter();

  expect(action).toEqual({
    type: 'SET_TEXT_FILTER',
    text: '',
  });
});
