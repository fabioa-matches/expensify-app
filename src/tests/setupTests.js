import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
// eslint-disable-next-line import/no-extraneous-dependencies
import DotEnv from 'dotenv';

Enzyme.configure({
  adapter: new Adapter(),
});

DotEnv.config({ path: '.env.test' });
