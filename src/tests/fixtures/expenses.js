import moment from 'moment';

export default [
  {
    id: '1',
    description: 'Cheese',
    note: '',
    amount: 112,
    createdAt: 86400,
  },
  {
    id: '2',
    description: 'Crackers',
    note: '',
    amount: 2150,
    createdAt: moment(86400).subtract(4, 'days').valueOf(),
  },
  {
    id: '3',
    description: 'Salt',
    note: '',
    amount: 265,
    createdAt: moment(86400).add(4, 'days').valueOf(),
  },
];
