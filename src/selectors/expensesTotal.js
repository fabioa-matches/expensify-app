const getExpensesTotal = expenses => expenses
  .map(x => x.amount)
  .reduce((acc, curr) => acc + curr, 0);

export default getExpensesTotal;
